// Project header
#include "circledetector.h"
#include "matfformconstants.h"

// STL headers
#include <utility>
#include <stdexcept>
#include <iostream>

// OpenCV header
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

CircleDetector::CircleDetector(const Mat &img, QObject *parent)
  : QObject(parent), img_(img)
{
}

std::vector<Vec3f> CircleDetector::detect()
{
  // Blur original image
  medianBlur(img_, img_, 3);

  // Threshold the image, keep only the blue pixels
  for (int i = 0; i < img_.rows; ++i)
  {
    for (int j = 0; j < img_.cols; ++j)
    {
      Vec3b pixel = img_.at<Vec3b>(i, j);
      int blue = pixel.val[0];
      int green = pixel.val[1];
      if (blue - green < 25)
        img_.at<Vec3b>(i, j) = Vec3b(0, 0, 0);
      else
        img_.at<Vec3b>(i, j) = Vec3b(255, 255, 255);
    }
  }

  Mat newImg;
  cvtColor(img_, newImg,CV_BGR2GRAY);

  // Use the Hough transform to detect circles in cropped image
  GaussianBlur(newImg, newImg, cv::Size(9, 9), 2, 2);
  vector<cv::Vec3f> circles;
  HoughCircles(newImg, circles, CV_HOUGH_GRADIENT, 1, 200, 100, 36);

  return move(circles);
}
