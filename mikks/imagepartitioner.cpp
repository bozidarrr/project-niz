#include "imagepartitioner.h"
#include "matfformconstants.h"

#include <stdexcept>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <iterator>

#include <QDateTime>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

ImagePartitioner::ImagePartitioner(const QString &pathToImg, bool filter, QObject *parent)
  : QObject(parent)
{
  // Read original image
  Mat fullImg = imread(pathToImg.toStdString());
  if (!fullImg.data)
    throw runtime_error("Couldn't read scan on given path.");

  // Get header rectangle
  RotatedRect headerRect = findHeaderRect(fullImg);

  // Rotate scan to be as flat as possible
  Mat r = move(straightenScan(fullImg, headerRect));

  // Find right position of rotated header vertices
  vector<Point2f> vertices(4);
  headerRect.points(&vertices.front());
  cv::transform(vertices, vertices, r);

  // Set top left corner to be first vertex
  iter_swap(vertices.begin(), min_element(vertices.begin(), vertices.end(), [=](const Point2f&l, const Point2f& r) { return l.x+l.y < r.x+r.y; }));

  // Init reference point
  topLeftCorner_ = vertices.front();

  // Init part of image that has circles
  initCirclesImg(fullImg, vertices.front());

  // Init application id digits
  initApplicationIdDigits(fullImg, vertices.front());

  // Init exam id digits
  initExamIdDigits(fullImg, vertices.front());

  // Init JMBG digits
  initJMBGDigits(fullImg, vertices.front());

  // Prepare digits
  if (filter)
    prepareDigits();
}

bool ImagePartitioner::writeDigits(const QString &baseDir, unsigned startIdx) const
{
  for (const auto& d : digits_)
    if (!imwrite((baseDir + "/" + QString::number(startIdx++) + ".jpg").toStdString(), d, {IMWRITE_JPEG_QUALITY, 100}))
      return false;
  return true;
}

RotatedRect ImagePartitioner::findHeaderRect(Mat& fullImg) const
{
  // Get region of interest in grayscale
  Mat regionOfInterest = fullImg(Rect(0, 0, fullImg.cols, fullImg.rows*.33333));
  Mat grayscaleRoi;
  cvtColor(regionOfInterest, grayscaleRoi, COLOR_BGR2GRAY);

  // Apply canny to grayscale image
  threshold(grayscaleRoi, grayscaleRoi, 125, 255, THRESH_BINARY_INV);
  dilate(grayscaleRoi, grayscaleRoi, Mat());

  // Find contours
  vector<vector<Point>> contours;
  vector<Vec4i> hierarchy;
  findContours(grayscaleRoi, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

  // Check that we have at least one contour
  if (contours.empty())
    throw runtime_error("Couldn't find any contour in image! Line: " LINE(__LINE__) " File: " __FILE__);

  // Find the biggest contour
  RotatedRect header;
  for (const auto& c : contours)
  {
    RotatedRect current = minAreaRect(c);
    Size2f currentSize = current.size;
    Size2f headerSize = header.size;
    if (currentSize.width*currentSize.height > headerSize.width*headerSize.height)
      header = current;
  }
  return move(header);
}

Mat ImagePartitioner::straightenScan(const Mat& fullImg, const RotatedRect &headerRect)
{
  double angle = headerRect.angle;
  if (headerRect.size.width < headerRect.size.height)
  {
    angle += (angle < 0) ? 90 : -90;
    scale_ = headerRect.size.height / MatfFormConstants::HEADER_WIDTH;
  }
  else
  {
    scale_ = headerRect.size.width / MatfFormConstants::HEADER_WIDTH;
  }
  Point2f pt(fullImg.cols/2., fullImg.rows/2.);
  Mat r = getRotationMatrix2D(pt, angle, 1.0);
  warpAffine(fullImg, fullImg, r, Size(fullImg.cols, fullImg.rows), INTER_LINEAR, BORDER_CONSTANT, Scalar::all(255));
  return move(r);
}

void ImagePartitioner::initCirclesImg(const Mat& fullImg, const Point2f &topLeftCorner)
{
  auto xstart = MatfFormConstants::COLX_DIST[0]*scale_ + topLeftCorner.x;
  auto ystart = MatfFormConstants::ROWY_DIST[0]*scale_ + topLeftCorner.y;
  auto xend = (MatfFormConstants::COLX_DIST.back() + MatfFormConstants::COL_SEG_DIST)*scale_ + topLeftCorner.x;
  auto yend = MatfFormConstants::ROWY_DIST.back()*scale_ + topLeftCorner.y;
  dy_ = -ystart;
  circlesImg_ = fullImg(Rect(xstart, ystart, xend-xstart, yend-ystart));
}

void ImagePartitioner::initApplicationIdDigits(const Mat &fullImg, const Point2f& topLeftCorner)
{
  digits_.reserve(24);
  auto xstart = MatfFormConstants::APPLICATION_ID_DELTA_X*scale_ + topLeftCorner.x;
  auto ystart = MatfFormConstants::APPLICATION_ID_DELTA_Y*scale_ + topLeftCorner.y;
  auto deltax = MatfFormConstants::APPLICATION_ID_BOX_WIDTH * scale_;
  auto deltay = MatfFormConstants::APPLICATION_ID_BOX_HEIGHT * scale_;
  auto linew = MatfFormConstants::APPLICATION_ID_LINE_WIDTH * scale_;
  do {
    digits_.push_back(fullImg(Rect(xstart-linew, ystart-linew, deltax+2*linew, deltay+2*linew)));
    xstart += deltax;
    xstart += linew;
  } while (digits_.size() < MatfFormConstants::NUM_OF_APPLICATION_ID_BOXES);
}

void ImagePartitioner::initExamIdDigits(const Mat &fullImg, const Point2f &topLeftCorner)
{
  auto xstart = MatfFormConstants::EXAM_ID_DELTA_X*scale_ + topLeftCorner.x;
  auto ystart = MatfFormConstants::EXAM_ID_DELTA_Y*scale_ + topLeftCorner.y;
  auto deltax = MatfFormConstants::EXAM_ID_BOX_WIDTH * scale_;
  auto deltay = MatfFormConstants::EXAM_ID_BOX_HEIGHT * scale_;
  auto linew = MatfFormConstants::EXAM_ID_LINE_WIDTH * scale_;
  do {
    digits_.push_back(fullImg(Rect(xstart-linew, ystart-linew, deltax+2*linew, deltay+2*linew)));
    xstart += deltax;
    xstart += linew;
  } while (digits_.size() < MatfFormConstants::NUM_OF_EXAM_ID_BOXES + MatfFormConstants::NUM_OF_APPLICATION_ID_BOXES);
}

void ImagePartitioner::initJMBGDigits(const Mat &fullImg, const Point2f &topLeftCorner)
{
  auto xstart = MatfFormConstants::JMBG_DELTA_X*scale_ + topLeftCorner.x;
  auto ystart = MatfFormConstants::JMBG_DELTA_Y*scale_ + topLeftCorner.y;
  auto deltax = MatfFormConstants::JMBG_BOX_WIDTH * scale_;
  auto deltay = MatfFormConstants::JMBG_BOX_HEIGHT * scale_;
  auto linew = MatfFormConstants::JMBG_LINE_WIDTH * scale_;
  do {
    digits_.push_back(fullImg(Rect(xstart-linew, ystart-linew, deltax+2*linew, deltay+2*linew)));
    xstart += deltax;
    xstart += linew;
  } while (digits_.size() < MatfFormConstants::NUM_OF_EXAM_ID_BOXES + MatfFormConstants::NUM_OF_APPLICATION_ID_BOXES + MatfFormConstants::NUM_OF_JMBG_BOXES);
}

void ImagePartitioner::prepareDigits()
{
  cleanDigits();
}

#ifdef DEBUG
void ImagePartitioner::displayImage(const Mat &image, bool writeImg, const string& name) const
{
  if (writeImg)
    imwrite(name, image);

  namedWindow("Digit", CV_WINDOW_NORMAL);
  imshow("Digit", image);
  waitKey();
}

void ImagePartitioner::drawRotatedRect(Mat &img, const RotatedRect &header) const
{
  Point2f vertices[4];
  header.points(vertices);
  line(img, vertices[0], vertices[1], Scalar(0, 0, 255), 4);
  line(img, vertices[1], vertices[2], Scalar(0, 0, 255), 4);
  line(img, vertices[2], vertices[3], Scalar(0, 0, 255), 4);
  line(img, vertices[3], vertices[0], Scalar(0, 0, 255), 4);
}
#endif

void ImagePartitioner::cleanDigit(Mat& img)
{
  int widthPadding = img.cols * .18;
  int heightPadding = img.rows * .18;

  // Up padding
  for (int i = 0; i < heightPadding; ++i)
  {
    for (int j = 0; j < img.cols; ++j)
    {
      Vec3b pixel = img.at<Vec3b>(i, j);
      int blue = pixel.val[0];
      int green = pixel.val[1];
      if (blue - green < 25)
        img.at<Vec3b>(i, j) = Vec3b(255, 255, 255);
    }
  }

  // Bottom padding
  for (int i = img.rows - heightPadding; i < img.rows; ++i)
  {
    for (int j = 0; j < img.cols; ++j)
    {
      Vec3b pixel = img.at<Vec3b>(i, j);
      int blue = pixel.val[0];
      int green = pixel.val[1];
      if (blue - green < 25)
        img.at<Vec3b>(i, j) = Vec3b(255, 255, 255);
    }
  }

  // Left padding
  for (int i = heightPadding; i < img.rows-heightPadding; ++i)
  {
    for (int j = 0; j < widthPadding; ++j)
    {
      Vec3b pixel = img.at<Vec3b>(i, j);
      int blue = pixel.val[0];
      int green = pixel.val[1];
      if (blue - green < 25)
        img.at<Vec3b>(i, j) = Vec3b(255, 255, 255);
    }
  }

  // Right padding
  for (int i = heightPadding; i < img.rows-heightPadding; ++i)
  {
    for (int j = img.cols-widthPadding; j < img.cols; ++j)
    {
      Vec3b pixel = img.at<Vec3b>(i, j);
      int blue = pixel.val[0];
      int green = pixel.val[1];
      if (blue - green < 25)
        img.at<Vec3b>(i, j) = Vec3b(255, 255, 255);
    }
  }

  // Central part
  for (int i = heightPadding; i < img.rows-heightPadding; ++i)
  {
    for (int j = widthPadding; j < img.cols-widthPadding; ++j)
    {
      Vec3b pixel = img.at<Vec3b>(i, j);
      int blue = pixel.val[0];
      int green = pixel.val[1];
      if (blue - green < 20 && !(pixel.val[0] < 100 && pixel.val[1] < 100 && pixel.val[2] < 100))
        img.at<Vec3b>(i, j) = Vec3b(255, 255, 255);
    }
  }
}

void ImagePartitioner::cleanDigits()
{
  for (auto& digit : digits_)
    cleanDigit(digit);
}

void ImagePartitioner::averagePixels(Mat &img)
{
  // Encode image to memory
  vector<uchar> imgBuff;
  imencode(".jpg", img, imgBuff, {});

  // Decode it again to original Mat
  img = imdecode(imgBuff, CV_LOAD_IMAGE_COLOR);
}

void ImagePartitioner::averageImgs()
{
  for (auto& digit : digits_)
    averagePixels(digit);
}
