#include "matfformconstants.h"

using namespace std;

const vector<unsigned> MatfFormConstants::COLX_DIST = {960-501, 1181-501, 1395-501, 1617-501, 1834-501, 2044-501, 2259-501};
const vector<unsigned> MatfFormConstants::ROWY_DIST = {4108-550, 4308-550, 4528-550, 4758-550, 4998-550, 5240-550, 5485-550, 5727-550, 5968-550, 6210-550, 6420-550};

const std::vector<unsigned> MatfFormConstants::COLX_DELTA = {0,
                                                                    MatfFormConstants::COLX_DIST[1] - MatfFormConstants::COLX_DIST[0],
                                                                    MatfFormConstants::COLX_DIST[2] - MatfFormConstants::COLX_DIST[1],
                                                                    MatfFormConstants::COLX_DIST[3] - MatfFormConstants::COLX_DIST[2],
                                                                    MatfFormConstants::COLX_DIST[4] - MatfFormConstants::COLX_DIST[3],
                                                                    MatfFormConstants::COLX_DIST[5] - MatfFormConstants::COLX_DIST[4],
                                                                    MatfFormConstants::COLX_DIST[6] - MatfFormConstants::COLX_DIST[5]
                                                                    };
const std::vector<unsigned> MatfFormConstants::ROWY_DELTA = {0,
                                                                   MatfFormConstants::ROWY_DIST[1]-MatfFormConstants::ROWY_DIST[0],
                                                                   MatfFormConstants::ROWY_DIST[2]-MatfFormConstants::ROWY_DIST[1],
                                                                   MatfFormConstants::ROWY_DIST[3]-MatfFormConstants::ROWY_DIST[2],
                                                                   MatfFormConstants::ROWY_DIST[4]-MatfFormConstants::ROWY_DIST[3],
                                                                   MatfFormConstants::ROWY_DIST[5]-MatfFormConstants::ROWY_DIST[4],
                                                                   MatfFormConstants::ROWY_DIST[6]-MatfFormConstants::ROWY_DIST[5],
                                                                   MatfFormConstants::ROWY_DIST[7]-MatfFormConstants::ROWY_DIST[6],
                                                                   MatfFormConstants::ROWY_DIST[8]-MatfFormConstants::ROWY_DIST[7],
                                                                   MatfFormConstants::ROWY_DIST[9]-MatfFormConstants::ROWY_DIST[8],
                                                                   MatfFormConstants::ROWY_DIST[10]-MatfFormConstants::ROWY_DIST[9]};
