#include "numextractor.h"

void NumExtractor::rgb2cmyk(cv::Mat& img, std::vector<cv::Mat>& cmyk) {
    // Allocate cmyk to store 4 componets
    for (int i = 0; i < 4; i++) {
        cmyk.push_back(cv::Mat(img.size(), CV_8UC1));
    }

    // Get rgb
    std::vector<cv::Mat> rgb;
    cv::split(img, rgb);

    // rgb to cmyk
    for (int i = 0; i < img.rows; i++) {
        for (int j = 0; j < img.cols; j++) {
            float r = (int)rgb[2].at<uchar>(i, j) / 255.;
            float g = (int)rgb[1].at<uchar>(i, j) / 255.;
            float b = (int)rgb[0].at<uchar>(i, j) / 255.;
            float k = std::min(std::min(1- r, 1- g), 1- b);

            cmyk[0].at<uchar>(i, j) = (1 - r - k) / (1 - k) * 255.;
            cmyk[1].at<uchar>(i, j) = (1 - g - k) / (1 - k) * 255.;
            cmyk[2].at<uchar>(i, j) = (1 - b - k) / (1 - k) * 255.;
            cmyk[3].at<uchar>(i, j) = k * 255.;
        }
    }
}

cv::Mat NumExtractor::prepareImage(cv::Mat img1){
    int min = 200;
    int max = 255;
    cv::Mat img = img1;

    // Blur original image
    medianBlur(img, img, 3);

    // Threshold the image, keep only the blue pixels
    for (int i = 0; i < img.rows; ++i)
    {
      for (int j = 0; j < img.cols; ++j)
      {
        Vec3b pixel = img.at<Vec3b>(i, j);
        int blue = pixel.val[0];
        int green = pixel.val[1];
        if (blue - green < 70) //if not blue, make it white
          img.at<Vec3b>(i, j) = Vec3b(255, 255, 255);
      }
    }

    cv::Mat newImg;
    cvtColor(img, newImg,CV_BGR2GRAY);

    cv::threshold(newImg,newImg, min, max, CV_THRESH_BINARY);
    bitwise_not(newImg, newImg);
    return newImg;
}

bool comp (Rect i,Rect j) { return (i.x<j.x); }

cv::Mat NumExtractor::prepareDataSVM(cv::Mat & other){
    resize(other, other, Size(28,28));
    cv::Size s = other.size();
    int width = (int)s.width;
    int height = (int)s.height;
    cv::Mat ret = other.clone();
    cv::Mat mat(1,width*height, CV_32F);

    for(int i = 0; i < height; ++i)
    {
        for(int j = 0; j < width; ++j)
        {
            mat.at<float>(i*width+j) = (float)other.at<uchar>(i,j);
        }
    }

    mat.reshape(1,1);
    return mat;
}

NumExtractor::NumExtractor(){
        predictor = Algorithm::load<cv::ml::SVM>(svmXMLPath);
        //trainingSet = cv::Mat(500,784,CV_32F);
}

// This algorithm could be optimized, to work in-place, with at most one copy
// If the other operations prove to be the bottleneck, it can stay as is.
cv::Mat NumExtractor::centerImage(cv::Mat img)
{
    int width = img.size().width;
    int height = img.size().height;

    int right = 0;
    int top = height - 1;
    int bottom = 0;

    // getting the borders for cropping the image
    // from the top, bottom and right side
    //
    for(int i = 0; i < width; ++i)
    {
        for(int j = 0; j < height; ++j)
        {
            if(img.at<uchar>(j,i) == 255)
            {
                if(i > right) right = i;
                if(j < top) top = j;
                if(j > bottom) bottom = j;
            }
        }
    }

    // cropping the image by saving it in a copy
    //
    int croppedWidth = right+1;
    int croppedHeight = bottom-top+1;

  //  std::cout << croppedWidth << endl <<croppedHeight << endl << right <<endl << top <<endl << bottom <<endl << "---------" << endl;

    Rect cropper(0,top,croppedWidth,croppedHeight);
    cv::Mat cropped = img(cropper);

    cv::Mat squared(croppedHeight,croppedHeight, CV_8U);
    squared = cv::Scalar(0,0,0,0);
    // If the cropped image is landscape-like, it should be cropped again to be a square
    //
    if(croppedWidth>croppedHeight)
    {
        for(int i = 0; i < croppedHeight; ++i)
        {
            for(int j = 0 ; j < croppedHeight; ++j)
            {
               squared.at<uchar>(j,i) = img.at<uchar>(j,i+croppedWidth-croppedHeight);
            }
        }
    }
    // If the cropped image is portrait-like, it should be extended on the left side
    //
    else if(croppedWidth<=croppedHeight)
    {
        int difference = croppedHeight-croppedWidth;
        for(int i = 0; i < croppedHeight; ++i)
        {
            for(int j = 0 ; j < croppedHeight; ++j)
            {
               if(i>=difference)
               {
                  squared.at<uchar>(j,i) = img.at<uchar>(j,i-difference);
               }
               else
               {
                  squared.at<uchar>(j,i) = 0;
               }
            }
        }
    }

    cv::Mat final;
    resize(squared, final, Size(NumExtractor::SIZE,NumExtractor::SIZE));

    return final;
}

std::string NumExtractor::extractNumber(std::vector<cv::Mat> digits){
    std::string res{""};
    for(cv::Mat digit : digits){
        cv::Mat temp = prepareImage(digit);
        cv::Mat temp2 = centerImage(temp);
        cv::Mat temp3 = prepareDataSVM(temp2);

        static int i=0;
        if((i%72)<50)
        for (int l=0;l<784;l++)
            trainingSet[((int)i/72)*50 + i%72][l]=temp3.at<float>(0,l);
        ++i;
        switch(i){
        case 203:
            for (int l=0;l<784;l++)
                trainingSet[500][l]=temp3.at<float>(0,l);
            break;
        case 208:
            for (int l=0;l<784;l++)
                trainingSet[501][l]=temp3.at<float>(0,l);
            break;
        case 275:
            for (int l=0;l<784;l++)
                trainingSet[502][l]=temp3.at<float>(0,l);
            break;
        case 340:
            for (int l=0;l<784;l++)
                trainingSet[503][l]=temp3.at<float>(0,l);
            break;
        case 343:
            for (int l=0;l<784;l++)
                trainingSet[504][l]=temp3.at<float>(0,l);
            break;
        case 344:
            for (int l=0;l<784;l++)
                trainingSet[505][l]=temp3.at<float>(0,l);
            break;
        case 411:
            for (int l=0;l<784;l++)
                trainingSet[506][l]=temp3.at<float>(0,l);
            break;
        case 423:
            for (int l=0;l<784;l++)
                trainingSet[507][l]=temp3.at<float>(0,l);
            break;
        case 425:
            for (int l=0;l<784;l++)
                trainingSet[508][l]=temp3.at<float>(0,l);
            break;
        case 427:
            for (int l=0;l<784;l++)
                trainingSet[509][l]=temp3.at<float>(0,l);
            break;
        case 428:
            for (int l=0;l<784;l++)
                trainingSet[510][l]=temp3.at<float>(0,l);
            break;
        case 430:
            for (int l=0;l<784;l++)
                trainingSet[511][l]=temp3.at<float>(0,l);
            break;
        case 431:
            for (int l=0;l<784;l++)
                trainingSet[512][l]=temp3.at<float>(0,l);
            break;
        case 432:
            for (int l=0;l<784;l++)
                trainingSet[513][l]=temp3.at<float>(0,l);
            break;
        case 485:
            for (int l=0;l<784;l++)
                trainingSet[514][l]=temp3.at<float>(0,l);
            break;
        case 486:
            for (int l=0;l<784;l++)
                trainingSet[515][l]=temp3.at<float>(0,l);
            break;
        case 492:
            for (int l=0;l<784;l++)
                trainingSet[516][l]=temp3.at<float>(0,l);
            break;
        case 500:
            for (int l=0;l<784;l++)
                trainingSet[517][l]=temp3.at<float>(0,l);
            break;
        case 563:
            for (int l=0;l<784;l++)
                trainingSet[518][l]=temp3.at<float>(0,l);
            break;
        case 628:
            for (int l=0;l<784;l++)
                trainingSet[519][l]=temp3.at<float>(0,l);
            break;
        case 631:
            for (int l=0;l<784;l++)
                trainingSet[520][l]=temp3.at<float>(0,l);
            break;
        case 632:
            for (int l=0;l<784;l++)
                trainingSet[521][l]=temp3.at<float>(0,l);
            break;
        case 638:
            for (int l=0;l<784;l++)
                trainingSet[522][l]=temp3.at<float>(0,l);
            break;
        case 637:
            for (int l=0;l<784;l++)
                trainingSet[523][l]=temp3.at<float>(0,l);
            break;
        case 642:
            for (int l=0;l<784;l++)
                trainingSet[524][l]=temp3.at<float>(0,l);
            break;
        case 643:
            for (int l=0;l<784;l++)
                trainingSet[525][l]=temp3.at<float>(0,l);
            break;
        case 644:
            for (int l=0;l<784;l++)
                trainingSet[526][l]=temp3.at<float>(0,l);
            break;
        case 645:
            for (int l=0;l<784;l++)
                trainingSet[527][l]=temp3.at<float>(0,l);
            break;
        case 647:
            for (int l=0;l<784;l++)
                trainingSet[528][l]=temp3.at<float>(0,l);
            break;
        case 648:
            for (int l=0;l<784;l++)
                trainingSet[529][l]=temp3.at<float>(0,l);
            break;
        case 701:
            for (int l=0;l<784;l++)
                trainingSet[530][l]=temp3.at<float>(0,l);
            break;
        case 702:
            for (int l=0;l<784;l++)
                trainingSet[531][l]=temp3.at<float>(0,l);
            break;
        case 704:
            for (int l=0;l<784;l++)
                trainingSet[532][l]=temp3.at<float>(0,l);
            break;
        case 707:
            for (int l=0;l<784;l++)
                trainingSet[533][l]=temp3.at<float>(0,l);
            break;
        case 708:
            for (int l=0;l<784;l++)
                trainingSet[534][l]=temp3.at<float>(0,l);
            break;
        case 712:
            for (int l=0;l<784;l++)
                trainingSet[535][l]=temp3.at<float>(0,l);
            break;
        }

        int a =(int)predictor->predict(prepareDataSVM(temp2));
        res += to_string(a);
    }
    return res;
}

std::string NumExtractor::readAppId(std::vector<cv::Mat> digits){
    if(digits.size() != 24){
        throw "This is an invalid array of digits!";
    }
    return extractNumber(std::vector<cv::Mat>(digits.begin(),digits.begin()+4));
}

std::string NumExtractor::readExamId(std::vector<cv::Mat> digits){
    if(digits.size() != 24){
        throw "This is an invalid array of digits!";
    }
    return extractNumber(std::vector<cv::Mat>(digits.begin()+4,digits.begin()+11));
}

std::string NumExtractor::readJmbgId(std::vector<cv::Mat> digits){
    if(digits.size() != 24){
        throw "This is an invalid array of digits!";
    }
    return extractNumber(std::vector<cv::Mat>(digits.begin()+11,digits.end()));
}
void NumExtractor::trainData(){
    Mat trainingDataMat(536, 784, CV_32FC1, trainingSet);

    predictor->setType(ml::SVM::C_SVC);
    predictor->setKernel(ml::SVM::POLY);
    predictor->setC(1);
    predictor->setGamma(2.5);
    predictor->setDegree(1.5);
    int odgovori[536];
    for(int j=0;j<500;j++)
    {
       odgovori[j]=((int)j/50);
    }

    odgovori[500]=odgovori[501]=2;
    odgovori[502]=3;
    odgovori[503]=odgovori[504]=odgovori[505]=4;
    odgovori[506]=odgovori[507]=odgovori[508]=odgovori[509]=odgovori[510]=odgovori[511]=odgovori[512]=odgovori[513]=5;
    odgovori[514]=odgovori[515]=odgovori[516]=odgovori[517]=6;
    odgovori[518]=7;
    odgovori[519]=odgovori[520]=odgovori[521]=odgovori[522]=odgovori[523]=odgovori[524]=odgovori[525]=odgovori[526]=odgovori[527]=odgovori[528]=odgovori[529]=8;
    odgovori[530]=odgovori[531]=odgovori[532]=odgovori[533]=odgovori[534]=odgovori[535]=9;
    cv::Mat responses(536, 1, CV_32SC1, odgovori);
    predictor->train(trainingDataMat,cv::ml::ROW_SAMPLE,responses);
    predictor->save(svmXMLPath);
}
