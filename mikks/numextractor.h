#ifndef __NUM_EXTRACTOR__
#define __NUM_EXTRACTOR__ 1


/*
 *  Class NumExtractor.
 *	Implementation of positioning on ExamCode and Application code of entry
 *	exam. It is specifically made for format of entry exam result list
 *	for Faculty of Mathematics in Belgrade.
 *	Using OpenCV and SVM to recognize digits.
 *
 * 	Because classifiers need to be loaded before any recognition made
 * 	class is implemented as singleton to prevent any necessary classifier
 * 	loading, because classifier's can be big (SVM classifier is ~24MB)
 *
 *
 */

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/ml.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/objdetect.hpp>
#include <opencv/cv.h>
#include <cstdio>
#include <vector>
#include <fstream>
#include <iostream>
#include <string>
using namespace std;
using namespace cv;

class NumExtractor{
public:
    // Get instance
    static NumExtractor& getInstance(){

        static NumExtractor instance;
        return instance;
    }
    // Static size of the output extracted image
    static const int SIZE = 150;
    // API for reading target data
    std::string readAppId(std::vector<cv::Mat> digits);
    std::string readExamId(std::vector<cv::Mat> digits);
    std::string readJmbgId(std::vector<cv::Mat> digits);
    void trainData();

private:
    // Preparing image for recognition (blurring, filtering)
    cv::Mat prepareImage(cv::Mat img);
    // Transforming image to "single vector" for purposes of SVM predictor
    cv::Mat prepareDataSVM(cv::Mat & other);
    // Reading numbers
    std::string extractNumber(std::vector<cv::Mat> digits);
    // SelfExplanatory
    cv::Mat centerImage(cv::Mat img);
    void rgb2cmyk(cv::Mat& img, std::vector<cv::Mat>& cmyk);
    // Singleton
    NumExtractor();
    NumExtractor(NumExtractor const&)    = delete;
  void operator=(NumExtractor const&)  = delete;

    // Necessary classifiers
    Ptr<cv::ml::SVM> predictor;
    float trainingSet[536][784];// = cv::Mat(500,784,CV_32F);

    int c = 0;
    // Paths
    std::string svmXMLPath = "../mikks/classifiers/classifier.xml";
};

#endif //__NUM_EXTRACTOR__
