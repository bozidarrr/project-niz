#include <QtWidgets>
#include <QtNetwork>
#include <iostream>

#include "window.h"
#include "examsolution.h"
#include "numextractor.h"


using namespace std;

Window::Window(QWidget *parent)
    : QWidget(parent), manager(new QNetworkAccessManager())
{
    browseButton = createButton(tr("&Pretrazi..."), SLOT(browse()));
    findButton = createButton(tr("&Zapocni pregledanje"), SLOT(startChecking()));
    findButton->setEnabled(false);

    directoryComboBox = createComboBox(QDir::homePath());

    fileLabel = new QLabel(tr("Svi pronadjeni fajlovi:"));
    directoryLabel = new QLabel(tr("Datoteka sa slikama:"));
    filesFoundLabel = new QLabel;

    rb300 = new QRadioButton("300dpi");
    rb600 = new QRadioButton("600dpi");
    rb1200 = new QRadioButton("1200dpi");

    createFilesTable();

    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->addWidget(fileLabel, 1, 0);
    mainLayout->addWidget(directoryLabel, 0, 0);
    mainLayout->addWidget(directoryComboBox, 0, 1);
    mainLayout->addWidget(browseButton, 0, 2);
    mainLayout->addWidget(filesTable, 3, 0, 1, 3);

    scanQualityLabel = new QLabel();
    scanQualityLabel->setText("Kvalitet skeniranja");

    QGridLayout *radioButtonLayout = new QGridLayout();
    radioButtonLayout->addWidget(rb300, 1,1);
    radioButtonLayout->addWidget(rb600, 1,2);
    radioButtonLayout->addWidget(rb1200, 1,3);

    mainLayout->addWidget(scanQualityLabel, 4,2);
    mainLayout->addLayout(radioButtonLayout, 5,2);
    mainLayout->addWidget(filesFoundLabel, 4, 0, 1, 2);
    mainLayout->addWidget(findButton, 6, 2);
    setLayout(mainLayout);

    setWindowTitle(tr("MATF - Prijemni ispit"));
    resize(700, 300);

    successfulCount = 0;
    errorCount = 0;
    rb300->setChecked(true);
}

void Window::browse()
{
    QString directory = QFileDialog::getExistingDirectory(this,
                                                          tr("Izbor direktorijuma"), QDir::homePath());

    if (!directory.isEmpty()){
        directoryComboBox->clear();
        directoryComboBox->setCurrentText(directory);
    }

    QString path = directoryComboBox->currentText();

    currentDir = QDir(path);
    QStringList filters;
    filters << "*.png" << "*.jpg" << "*.bmp" << "*.jpeg" << "*.gif";
    files = currentDir.entryList(filters,
                                 QDir::Files | QDir::NoSymLinks);

    //Reset all parameters
    filesTable->setRowCount(0);
    showFiles(files);
    QStringList labels;
    labels << tr("Ime fajla") << tr("Velicina");
    filesTable->setHorizontalHeaderLabels(labels);
    findButton->setEnabled(true);
    successfulCount = 0;
    errorCount = 0;
    fileLabel->setText("Svi fajlovi:");
}


void Window::showFiles(const QStringList &files)
{
    for (int i = 0; i < files.size(); ++i) {
        QFile file(currentDir.absoluteFilePath(files[i]));
        qint64 size = QFileInfo(file).size();

        QTableWidgetItem *fileNameItem = new QTableWidgetItem(files[i]);
        fileNameItem->setFlags(fileNameItem->flags() ^ Qt::ItemIsEditable);
        QTableWidgetItem *sizeItem = new QTableWidgetItem(tr("%1 KB")
                                                          .arg(int((size + 1023) / 1024)));
        sizeItem->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        sizeItem->setFlags(sizeItem->flags() ^ Qt::ItemIsEditable);

        int row = filesTable->rowCount();
        filesTable->insertRow(row);
        filesTable->setItem(row, 0, fileNameItem);
        filesTable->setItem(row, 1, sizeItem);
    }
    filesFoundLabel->setText(tr("Broj pronadjenih fajlova: %1").arg(files.size()) +
                             (" (Dvoklikom mozete otvoriti svaki fajl)"));
    filesFoundLabel->setWordWrap(true);
}

QPushButton *Window::createButton(const QString &text, const char *member)
{
    QPushButton *button = new QPushButton(text);
    connect(button, SIGNAL(clicked()), this, member);
    return button;
}

QComboBox *Window::createComboBox(const QString &text)
{
    QComboBox *comboBox = new QComboBox;
    comboBox->setEditable(true);
    comboBox->addItem(text);
    comboBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    return comboBox;
}

void Window::createFilesTable()
{
    filesTable = new QTableWidget(0, 2);
    filesTable->setSelectionBehavior(QAbstractItemView::SelectRows);

    QStringList labels;
    labels << tr("Ime fajla") << tr("Velicina");
    filesTable->setHorizontalHeaderLabels(labels);
    filesTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    filesTable->verticalHeader()->hide();
    filesTable->setShowGrid(false);

    connect(filesTable, SIGNAL(cellActivated(int,int)),
            this, SLOT(openFileOfItem(int,int)));
}

void Window::openFileOfItem(int row, int /* column */)
{
    QTableWidgetItem *item = filesTable->item(row, 0);

    QDesktopServices::openUrl(QUrl::fromLocalFile(currentDir.absoluteFilePath(item->text())));
}

void Window::sendPostRequest(QString file_path){
    ImagePartitioner imgp(file_path);
    QString answers = getAnswers(imgp);
    string appId = NumExtractor::getInstance().readAppId(imgp.digitImages());
    string examId = NumExtractor::getInstance().readExamId(imgp.digitImages());
    string jmbgId = NumExtractor::getInstance().readJmbgId(imgp.digitImages());
    int applicationId = stoi(appId);
    QString q_appId = appId.c_str();
    QString q_examId = examId.c_str();
    QString q_jmbgId = jmbgId.c_str();
    qDebug() << "Putanja: " << file_path << endl << "Licni broj: " << q_jmbgId << endl << "Broj prijave: " << q_appId << endl << "Sifra zadatka: " << q_examId << endl << "Odgovori: " << answers << endl;
    QNetworkRequest request;

    QSslConfiguration config = QSslConfiguration::defaultConfiguration();
    config.setProtocol(QSsl::TlsV1_2);
    request.setSslConfiguration(config);
    request.setUrl(QUrl("http://192.168.1.102/server/server.php"));
    request.setHeader(QNetworkRequest::ContentTypeHeader,QVariant("application/x-www-form-urlencoded"));

//    connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));

    QUrlQuery params;
    params.addQueryItem("answers", answers);
    params.addQueryItem("application_id", QString::number(applicationId));
    params.addQueryItem("exam_id", q_examId);
    params.addQueryItem("file_path", file_path);
    QNetworkReply* reply = manager->post(request, params.query(QUrl::FullyEncoded).toUtf8());
    connect(reply, SIGNAL(readyRead()), this, SLOT(replyFinished()));
}

void Window::replyFinished() {

    QNetworkReply* reply = (QNetworkReply*)QObject::sender();
    //auto val = progressDialog->value() + 1;
    //progressDialog->setValue(val);

    if (reply->error() == QNetworkReply::NoError) {

        fileLabel->setText("Neuspesno obradjeni fajlovi:");
        QString strReply = (QString)reply->readAll();

        //parse json
        QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());

        QJsonObject jsonObj = jsonResponse.object();

        if(jsonObj["error"].toBool()){
            qDebug() << "message:" << jsonObj["message"].toString();
            QString fileName = jsonObj["path"].toString();
            QFile file(currentDir.absoluteFilePath(fileName));

            QTableWidgetItem *fileNameItem = new QTableWidgetItem(fileName);
            fileNameItem->setFlags(fileNameItem->flags() ^ Qt::ItemIsEditable);

            QTableWidgetItem *sizeItem = new QTableWidgetItem(jsonObj["message"].toString());
            sizeItem->setTextAlignment(Qt::AlignCenter | Qt::AlignVCenter);
            sizeItem->setFlags(sizeItem->flags() ^ Qt::ItemIsEditable);


            int row = filesTable->rowCount();
            filesTable->insertRow(row);
            filesTable->setItem(row, 0, fileNameItem);
            filesTable->setItem(row, 1, sizeItem);
            errorCount++;
        }
        else{
            qDebug() << "Nema greske." << endl;
            successfulCount++;
        }

        filesFoundLabel->setText( "<font size=14><b>1111</b></font><font size=20>6666</font>" );
        filesFoundLabel->setText(tr("<pre> Ukupno fajlova: %1   ").arg(files.size()) +
                                 tr("<span style='color:green;'>Uspesno obradjenih: %1 </span>   ").arg(successfulCount) +
                                 tr("<span style='color:red;'>Neuspesno obradjenih: %1 </span>   </pre>").arg(errorCount));
        filesFoundLabel->setTextFormat( Qt::RichText );
        filesFoundLabel->setWordWrap(true);
    }
    reply->deleteLater();
}

QString Window::getAnswers(const ImagePartitioner& imgp)
{
  return ExamSolution(imgp.circlesImage(), imgp.scaleCoef())();
}

void Window::startChecking(){
    findButton->setEnabled(false);
    filesTable->setRowCount(0);
    QStringList labels;
    labels << tr("Putanja fo fajla") << tr("Poruka o gresci");
    filesTable->setHorizontalHeaderLabels(labels);
    QString path = directoryComboBox->currentText();

    currentDir = QDir(path);
    QStringList files;
    QStringList filters;
    filters << "*.png" << "*.jpg" << "*.bmp" << "*.jpeg" << "*.gif";
    files = currentDir.entryList(filters,
                                 QDir::Files | QDir::NoSymLinks);

    progressDialog = new QProgressDialog(tr("Procenat obradjenih fajlova"), "Odustani", 0, files.size(), this);
    progressDialog->setWindowModality(Qt::WindowModal);
    progressDialog->setValue(0);

    for (int i = 0; i < files.size(); ++i) {
        //request
        sendPostRequest(currentDir.absoluteFilePath(files[i]));
        progressDialog->setValue(i);
    }
    //NumExtractor::getInstance().trainData();
    progressDialog->setValue(files.size());
    directoryComboBox->setCurrentText("");
}
