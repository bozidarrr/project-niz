
#ifndef WINDOW_H
#define WINDOW_H

#include "imagepartitioner.h"

#include <QWidget>
#include <QDir>
#include <QtNetwork>
#include <QtWidgets>

using namespace std;

class QComboBox;
class QLabel;
class QPushButton;
class QTableWidget;
class QTableWidgetItem;

class Window : public QWidget
{
    Q_OBJECT

public:
    Window(QWidget *parent = 0);
    void sendPostRequest(QString file_path);
    QString getAnswers(const ImagePartitioner &imgp);
private slots:
    void browse();
    void startChecking();
    void openFileOfItem(int row, int column);
    void replyFinished();
private:
    void showFiles(const QStringList &files);
    QPushButton *createButton(const QString &text, const char *member);
    QComboBox *createComboBox(const QString &text = QString());
    void createFilesTable();

    QComboBox *directoryComboBox;
    QLabel *fileLabel;
    QLabel *directoryLabel;
    QLabel *filesFoundLabel;
    QPushButton *browseButton;
    QPushButton *findButton;
    QTableWidget *filesTable;

    QLabel *scanQualityLabel;
    QRadioButton *rb300;
    QRadioButton *rb600;
    QRadioButton *rb1200;

    QDir currentDir;
    QProgressDialog* progressDialog;
    int successfulCount;
    int errorCount;
    QStringList files;
    QNetworkAccessManager* manager;
};

#endif
