<style>
	table.my_table {
		font-family: verdana,arial,sans-serif;
		color:black;
		border: 1px solid black;
		border-collapse: collapse;
		font-size: 12px;
		width: 100%;
		box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
		display: table;
		}

	.my_table tr {
		display: table-row;
		background: #f6f6f6;
		border: 1px solid black;
		
	}

	.my_table tr:hover {
		display: table-row;
		background: #2980b9 !important;
		color:white;
	}

	tr:nth-of-type(odd) {
		background: #e9e9e9;
	}

	.my_table tr.header {
		font-family: verdana,arial,sans-serif;
		font-size: 12px;
		font-weight: bold;
		color: black;
		background: white !important;
		border: 1px solid black;

	}
	
	.my_table td {
		padding: 4px 7px;
		display: table-cell;
		border: 2px solid black;
	}
</style>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">
<br/>
<div style="min-width:100%;min-hight:40px; margin-left:10%; margin-top:30px;">
		<br/>
<b>Универзитет у Београду - Математички факултет <br/>
Коначна ранг листа кандидата у уписном року Јун <?php echo date('Y'); ?> </b><br/>
<p>Свим кандидатима који су конкурисали на афирмативној ранг листи
одобрен је упис преко афирмативне ранг листе.</p>
<p>Упис ће бити огранизован према распореду објављеном на сајту факултета (upis.matf.bg.ac.rs)</p><br/><button onclick="exportToPdf()"> PDF </button> &nbsp; &nbsp; 
<!-- <button onclick="exportToExcel()"> Excel </button> --></div>

<div id="table_div" style="margin-left:10%!important; margin-right:10%;margin-top:30px; margin-bottom:30px;"> 
</div>

<script type="text/javascript" src="jquery-1.11.3.js"></script>

<script type="text/javascript">
var html="";
$.ajax({
        url: "rang.php",
        type:'GET',
        success: function(result){
        	html=result;
         	$("#table_div").html(html);
            
        }
    });
function exportToPdf(){
$.ajax({
  	url: "pdf.php",
  	dataType: 'json',
    type:'POST',
    data:{'html':html},
  	success: function(result){
       window.location.href='/'+result;
  		
   }
  
 });
}
function exportToExcel(){
$.ajax({
  	url: "excel.php",
  	dataType: 'json',
    type:'POST',
    data:{'html':html},
  	success: function(result){
       window.location.href=result;
  		
   }
  
 });
}
</script>


